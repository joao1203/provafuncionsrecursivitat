//Añadir la entrada de dades en Program arguments
import java.util.*

const val bold = "\u001b[1m"
const val underline = "\u001b[4m"
const val reset = "\u001B[0m"

fun main(args: Array<String>){
    val scanner = Scanner(System.`in`)
    menu()
    var option: Int

    do {
        option = scanner.nextInt()
        choiceMenu(option,args)
        println("")
        if(option != 0){
            menu()
        }
    } while (option != 0)
}

fun menu(){
    println("$bold${underline}MENU:$reset")
    println("· Mostrar dades llegides de l’activitat                            [1]")
    println("· Mostrar quantitat mesures de l’activitat                         [2]")
    println("· Mostrar freqüència cardíaca mitjana de l’activitat               [3]")
    println("· Mostrar freqüències cardíaques mínima i màxima de l’activitat    [4]")
    println("· Sortir                                                           [0]")
}

fun choiceMenu (option: Int, args: Array<String>){
    when(option){
        1 -> showActivity(args)
        2 -> totalActivity(args)
        3 -> avgActivity(args)
        4 -> minmaxActivity(args)
        else -> {
            println("$option is not a valid option, try again:")
        }
    }

}

fun showActivity (args: Array<String>){
    for (i in args){
        print("$i ")
    }
    println()
}

fun totalActivity (args: Array<String>){
    var count = 0
    for (i in args){
        count++
    }
    println(count)
}

fun avgActivity (args: Array<String>){
    var count = 0
    var sum = 0
    for (i in args){
        sum += i.toInt()
        count++
    }
    println( sum / count)
}

fun minmaxActivity (args: Array<String>){
    var min = args[0].toInt()
    var max = args[0].toInt()

    for(i in args){
        val j = i.toInt()
        if (j < min) min = j
        if (j > max) max = j
    }
    println("Activitat Minima: $min Activitat Maxima: $max")
}