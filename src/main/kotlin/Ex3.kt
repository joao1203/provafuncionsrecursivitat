import java.util.*

const val ANSI_RESET = "\u001B[0m"
const val ANSI_BLACK = "\u001B[40m"
const val ANSI_RED = "\u001B[41m"
const val ANSI_GREEN = "\u001B[42m"
const val ANSI_YELLOW = "\u001B[43m"
const val ANSI_BLUE = "\u001B[44m"
const val ANSI_PURPLE = "\u001B[45m"
const val ANSI_CYAN = "\u001B[46m"
const val ANSI_WHITE = "\u001B[47m"
var offColor = 2

fun main(){
    val scanner = Scanner(System.`in`)
    val colorList = arrayOf(ANSI_BLACK, ANSI_WHITE, ANSI_RED, ANSI_GREEN, ANSI_YELLOW, ANSI_BLUE, ANSI_PURPLE, ANSI_CYAN)
    var color = 1
    var intensity = 1
    var intensityIncrease = true

    print("Introduce Order: ")

    do {
        var order = scanner.nextLine().uppercase()
        when(order) {
            "TURN ON" -> {
                if(color==0){
                    color = colorList[offColor].toInt()
                    intensity=1
                    intensityIncrease=true
                }
            }
            "TURN OFF" -> {
                if(color>0){
                    color = 0
                    intensity = 0
                    offColor = 0
                }
            }
            "CHANGE COLOR" -> {
                if (color<=colorList.size-2) color++
                else if(color==colorList.size-2) color=1
            }
            "INTENSITY" -> {
                if (intensityIncrease){ intensity++}
                else if (intensityIncrease && intensity==5){
                    intensityIncrease=false
                    intensity--
                } else if (!intensityIncrease && intensity>1) intensity--
                else if (!intensityIncrease && intensity==1){
                    intensity++
                }
            }
        }
        println("Color: ${colorList[color]}    $ANSI_RESET - intensitat $intensity")
    }while (order != "END")
}