import java.util.*
import kotlin.math.pow

fun main(){
    val scanner = Scanner(System.`in`)
    println("Type the number correspondent to the desired equation.")
    println("· Decimal to Binary    [0]")
    println("· Binary to Decimal    [1]")
    val option = scanner.nextInt()
    if (option == 0){
        println("Insert the value:")
        val num = scanner.nextInt()
        println(decToBin(num))
    }
    else if (option == 1){
        println("Insert the value:")
        val num = scanner.nextLong()
        println(binToDec(num))
    }
    else{
        println("Value entered is not supported.")
    }
}

fun decToBin (num: Int): Long{
    var num = num
    var binary: Long = 0
    var count = 0

    while (num != 0) {
        val left = num % 2
        var multiply = 10.toDouble().pow(count)
        binary += (left * multiply).toLong()
        num /= 2
        count++
    }
    return binary
}

fun binToDec (num:Long): Int{
    var num = num
    var decimal = 0
    var count = 0

    while (num.toInt() != 0) {
        var left: Long = num % 10
        num /= 10
        decimal += (left * 2.0.pow(count)).toInt()
        count++
    }
    return decimal
}